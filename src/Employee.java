public class Employee {
 //Thuộc tính Employee
 private int id;
 private String firstName;
 private String lastName;
 private int salary;
//Phương thức khởi tạo
 public Employee(int id, String firstName, String lastName, int salary) {
  this.id = id;
  this.firstName = firstName;
  this.lastName = lastName;
  this.salary = salary;
 }
 /**
  * getter method
  * @return 
  */
  public int getID() {
   return this.id;
  }

  public String getFirstName() {
   return this.firstName;
  }

  public String getLastName() {
   return this.lastName;
  }

  public String getName () {
   return this.firstName + " " + this.lastName;
  }

  public int getSalary() {
   return salary;
  }

  public int getAnualSalary() {
   return this.salary * 12;
  }
  /**
  * setter method
  * @return 
  */
  public void setSalary(int salary) {
   this.salary = salary;
  }

  public int raiseSalary(Integer percent) {
    float percentCal = (((float)(percent) / 100) + 1);
    // System.out.println(percentCal);
     return (int) ( this.salary * percentCal);
  }

  public String toString(){
   return "Employee:[id=" + id + ", name=" + firstName + " " + lastName + ", salary=" + salary + "]";
  }
}
